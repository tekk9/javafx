package event.handling;

import java.util.Optional;

import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Separator;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class FormView extends BorderPane
{
    Label lbl_country;
    TextField fld_country;

    Label lbl_city;
    TextField fld_city;

    Label lbl_state;
    TextField fld_state;

    Label lbl_postalCode;
    TextField fld_postalCode;

    GridPane rec_form;

    ListView<Country> lst_city;

    TableView<Country> tbl_person;
    TableColumn<Country, String> countryCol;
    TableColumn<Country, String> cityCol;
    TableColumn<Country, String> stateCol;
    TableColumn<Country, String> postalCodeCol;

    TextArea evt_info;

    MenuBar mnu_bar;
    MenuItem mit_file;
    MenuItem mit_dbase;
    MenuItem mit_exit;

    Button btn_add;
    Button btn_edit;
    Button btn_delete;
    Button btn_clear;
    Button btn_refresh;

    DataModel the_model;
    Alert alert;
    Alert error;

    public FormView(DataModel model)
    {
	initForm(model);
    }

    public void initForm(DataModel model)
    {
	this.setPadding(new Insets(10, 10, 10, 10));

	this.the_model = model;

	createMenuBar();
	createRecForm(model);
	createEvtInfo();
	createEmployeesTable(model);
	setEvtHandlers(model);
    }

    // ----------------------------------
    public void createRecForm(DataModel model)
    {
	VBox vbox = new VBox(10);
	{
	    rec_form = new GridPane();
	    {
		rec_form.setAlignment(Pos.TOP_LEFT);
		rec_form.setHgap(20);
		rec_form.setVgap(10);
		rec_form.setPadding(new Insets(25, 25, 25, 25));

		lbl_country = new Label("Country :");
		fld_country = new TextField();

		lbl_city = new Label("City :");
		fld_city = new TextField();

		lbl_state = new Label("State :");
		fld_state = new TextField();

		lbl_postalCode = new Label("PostalCode");
		fld_postalCode = new TextField();

		rec_form.add(lbl_country, 0, 0);
		rec_form.add(lbl_city, 0, 1);
		rec_form.add(lbl_state, 0, 2);
		rec_form.add(lbl_postalCode, 0, 3);

		rec_form.add(fld_country, 1, 0, 6, 1);
		rec_form.add(fld_city, 1, 1, 6, 1);
		rec_form.add(fld_state, 1, 2, 6, 1);
		rec_form.add(fld_postalCode, 1, 3, 6, 1);

		rec_form.setMinWidth(300);
	    }
	    vbox.getChildren().add(rec_form);

	    HBox btn_bar = new HBox(10);
	    {
		btn_add = new Button("Add");
		btn_edit = new Button("Edit");
		btn_delete = new Button("Delete");
		btn_clear = new Button("Clear");
		btn_refresh = new Button("Refresh");
		btn_bar.getChildren().addAll(btn_refresh, btn_add, btn_edit, btn_delete, btn_clear);
		btn_bar.setAlignment(Pos.TOP_CENTER);
	    }
	    vbox.getChildren().addAll(new Separator(), btn_bar);
	}

	this.setCenter(vbox);
    }

    // ---------------------

    @SuppressWarnings({ "rawtypes", "unchcked", })
    public void createEmployeesTable(DataModel model)
    {
	tbl_person = new TableView<Country>();
	{
	    tbl_person.setItems(model.getPersonNameData());
	    countryCol = new TableColumn("Country");
	    countryCol.setCellValueFactory(new PropertyValueFactory<Country, String>("country"));

	    cityCol = new TableColumn("City");
	    cityCol.setCellValueFactory(new PropertyValueFactory<Country, String>("city"));

	    stateCol = new TableColumn("State");
	    stateCol.setCellValueFactory(new PropertyValueFactory<Country, String>("state"));

	    postalCodeCol = new TableColumn("PostalCode");
	    postalCodeCol.setCellValueFactory(new PropertyValueFactory<Country, String>("postalCode"));

	}

	tbl_person.getColumns().addAll(countryCol, cityCol, stateCol, postalCodeCol);
	this.setLeft(tbl_person);
    }

    public void createMenuBar()
    {
	mnu_bar = new MenuBar();
	{

	    Menu mnu_file = new Menu("File");
	    {
		mit_file = new MenuItem("Open Text File");
		mit_dbase = new MenuItem("Open Data Base");
		mit_exit = new MenuItem("Exit");

		mnu_file.getItems().addAll(
			mit_file, mit_dbase, new SeparatorMenuItem(), mit_exit
		);
		mnu_bar.getMenus().add(mnu_file);
	    }
	}
	this.setTop(mnu_bar);
    }

    public void createEvtInfo()
    {
	evt_info = new TextArea();
	evt_info.setEditable(false);

    }

    public void printEvtInfo(String info)
    {
	evt_info.appendText(info);
    }

    public void setEvtHandlers(DataModel model)
    {

	mit_file.setOnAction((value) ->
	{
	    printEvtInfo("Menu Item Selected: Text File \n");
	    this.the_model = new DataModel(DataModel.Source.SRC_DBASE);
	    this.the_model.setData("WorldCities");
	    initForm(the_model);
	    printEvtInfo(">>> DataSource changed to the Data Base");
	});

	mit_file.setOnAction((value) ->
	{
	    printEvtInfo("Menu Item Selected: Data Base \n");
	    this.the_model = new DataModel(DataModel.Source.SRC_DBASE);
	    this.the_model.setData("customers");
	    initForm(the_model);
	    printEvtInfo(">>> DataSource changed to the Data Base");
	});

	mit_exit.setOnAction((value) ->
	{
	    printEvtInfo("Menu Item Selected: Exit \n");
	    Platform.exit();
	});

	tbl_person.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
	{
	    showPersonDetails(newValue);
	    tbl_person.setItems(model.getPersonNameData());
	    btn_edit.setOnAction(e ->
	    {
		printEvtInfo("Button pressed: Edit \n");
		DataModel.editEmployeesDetails(newValue.countryProperty().get(), newValue.cityProperty().get(),
			newValue.stateProperty().get(), newValue.postalCodeProperty().get(), fld_country.getText(),
			fld_city.getText(), fld_state.getText(), fld_postalCode.getText());
	    });
	});

	fld_country.textProperty().addListener((observable, oldValue, newValue) ->
	{
	    printEvtInfo("City - TextField Text Changed (new value: " + newValue + ")\n");
	});

	fld_country.setOnAction((event) ->
	{
	    printEvtInfo("City - TextField Action.\n");
	});

	btn_refresh.setOnAction((value) ->
	{
	    printEvtInfo("Button pressed: Refresh \n");
	    RefreshMethod();
	});

	btn_delete.setOnAction((value) ->
	{
	    printEvtInfo("Button pressed: Clear \n");
	    alert = new Alert(AlertType.CONFIRMATION);
	    alert.setTitle("Confiramation Dialog");
	    alert.setHeaderText("");
	    alert.setContentText("Are you sure want to delete this?");

	    String firstCol = "", secondCol = "", thirdCol = "", forthCol = "";
	    firstCol = this.fld_country.getText();
	    secondCol = this.fld_city.getText();
	    thirdCol = this.fld_state.getText();
	    forthCol = this.fld_postalCode.getText();

	    Optional<ButtonType> result = alert.showAndWait();
	    if (result.get() == ButtonType.OK)
	    {
		DataModel.deleteEmployeesDetails(firstCol, secondCol, thirdCol, forthCol);
		printEvtInfo("Button pressed: Delete confirmation true \n");
		ClearMethod();
	    } else
	    {
		printEvtInfo("Button pressed: Delete confirmation false \n");
		ClearMethod();
			RefreshMethod();
	    }
	});

	btn_clear.setOnAction((value) ->
	{
	    printEvtInfo("Button pressed: Clear \n");
	    ClearMethod();
	});

	btn_add.setOnAction((value) ->
	{
	    String firstCol = "", secondCol = "", thirdCol = "", forthCol = "";
	    if (this.fld_country.getText().equals(null) && this.fld_city.getText().equals(null)
		    && this.fld_state.getText().equals(null) && this.fld_postalCode.getText().equals(null))
	    {
		error = new Alert(AlertType.ERROR);
		alert.setTitle("Error Dialog");
		alert.setHeaderText("Look, an Error Dialog");
		alert.setContentText("Fill all text areas!");
		alert.showAndWait();
	    } else
	    {
		firstCol = this.fld_country.getText();
		secondCol = this.fld_city.getText();
		thirdCol = this.fld_state.getText();
		forthCol = this.fld_postalCode.getText();
		DataModel.addEmployeesDeatails(firstCol, secondCol, thirdCol, forthCol);
	    }

	    printEvtInfo("Button pressed: Add \n");
	    ClearMethod();
		RefreshMethod();

	});

    }

    public void NewValues(Country country)
    {

    }

    public void showPersonDetails(Country Country)
    {
	if (Country != null)
	{
	    this.fld_country.setText(Country.countryProperty().get());
	    this.fld_city.setText(Country.cityProperty().get());
	    this.fld_state.setText(Country.stateProperty().get());
	    this.fld_postalCode.setText(Country.postalCodeProperty().get());
	} else
	{
	    ClearMethod();
	}
    }

    public void ClearMethod()
    {
	this.fld_country.setText(null);
	this.fld_city.setText(null);
	this.fld_state.setText(null);
	this.fld_postalCode.setText(null);
    }

    public void RefreshMethod()
    {

	this.the_model = new DataModel(DataModel.Source.SRC_DBASE);
	this.the_model.setData("customers");
	initForm(the_model);
    }
}
