package event.handling;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Country
{
    public StringProperty country;
    public StringProperty city;
    public StringProperty state;
    public StringProperty postalCode;

    public Country(String country, String city, String state, String postalCode)
    {
	this.country = new SimpleStringProperty(country);
	this.city = new SimpleStringProperty(city);
	this.state = new SimpleStringProperty(state);
	this.postalCode = new SimpleStringProperty(postalCode);
    }

    public StringProperty countryProperty()
    {
	return country;
    }

    public StringProperty cityProperty()
    {
	return city;
    }

    public StringProperty stateProperty()
    {
	return state;
    }

    public StringProperty postalCodeProperty()
    {
	return postalCode;
    }

    // @Override
    public String toString()
    {
	return countryProperty().getValue();
    }
}